﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GunType {
    Standard,
	Laser,
    Missile
}
