﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Strings {
    public static class Resources {
        public static string BulletHitEffect = "BulletHitEffect";
        public static string Enemy = "Enemy";
        public static string Beam = "Beam";
        public static string Laser = "Laser";
        public static string Explosion = "Explosion";
        public static string Bullet = "Bullet";
        public static string EnemyBullet = "EnemyBullet";
        public static string HealthSlider = "HealthSlider";
        public static string ShieldSlider = "ShieldSlider";
        public static string Missile = "Missile";
    }
    public static class Tags {
        public static string Enemy = "Enemy";
        public static string Player = "Player";
        public static string Bullet = "Bullet";
    }
    public static class Layers {
        public static string Player = "Player";
        public static string PlayerBullet = "Bullet_Player";
        public static string Enemy = "Enemy";
        public static string EnemyBullet = "Bullet_Enemy";
    }
}
