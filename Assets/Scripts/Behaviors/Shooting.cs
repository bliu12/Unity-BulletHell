﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooting : MonoBehaviour {
    private float cooldown = 0;
	public BaseGun gun;
    public TargetingBase targeting;

    void Start () {
        if (targeting == null)
        {
            this.targeting = gameObject.AddComponent<MouseTargeting>();
        }
	}

    // Update is called once per frame
    void Update()
    {
        if (cooldown > 0)
        {
            cooldown -= Time.deltaTime;
        }
    }

    public void ShootGun()
    {
        if (this.gun != null && cooldown <= 0)
        {
            cooldown = 60 / (float)gun.shotsPerMinute;
            gun.Shoot(this.targeting);
        }
    }
    public void ReleaseGun()
    {
        gun.Release();
    }

    public void UpdateGun(BaseGun newGun)
    {
        gun = newGun;
    }
}
