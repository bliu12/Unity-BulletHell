﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Beam : MonoBehaviour {

    public TargetingBase targeting;
	LineRenderer line;
	GameObject player;
    private float maxLength = 40;
    public float length = 100;
    private float speed = 80;
    public float damage = 1000;
	public LayerMask layerMask;
	// Use this for initialization
	void Start () {
		this.line = GetComponent<LineRenderer>();

		player = GameObject.FindGameObjectWithTag(Strings.Tags.Player);
        // Initialize the laser inside the player and not prefab position.
        line.SetPosition(0, player.transform.position);

        line.SetPosition (1, player.transform.position);


	}
	
	// Update is called once per frame
	void Update () {
		
        // first point on the line
		line.SetPosition (0, player.transform.position);


        // the ray for the collision detection
        Vector2 worldPoint = targeting.GetTargetPoint();

        // the ray for the second point on the line
		Vector2 worldPoint2 = worldPoint - (Vector2) player.transform.position;

        if (length < maxLength) length += speed * Time.deltaTime;
        if (length > maxLength) length = maxLength;
		RaycastHit2D hitCast = Physics2D.Raycast(player.transform.position, worldPoint2, length, layerMask);

        if (hitCast) {
            length = (hitCast.point - (Vector2)player.transform.position).magnitude;
            var objectShields = hitCast.collider.gameObject.GetComponent<Shields>();
            var objectHealth = hitCast.collider.gameObject.GetComponent<HealthBar>();

            if (objectShields != null) {
                objectShields.Hit(damage * Time.deltaTime);
            }
            else if(objectHealth != null) {
                objectHealth.Hit(damage * Time.deltaTime);
			}
		} 
        line.SetPosition (1, (Vector2) player.transform.position + worldPoint2.normalized*length);
	}
}
