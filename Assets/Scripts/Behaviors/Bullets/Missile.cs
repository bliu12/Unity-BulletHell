﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Missile : Bullet {
    public TargetingBase targeting;
    // Use this for initialization
    private Rigidbody2D rigidBody;
	private float strengthOfAttraction = 10;
    public bool isTargeting = true;

    private float baseDamage;

    new void Start () {
        rigidBody = GetComponent<Rigidbody2D>();
        baseDamage = damage;
	}
	
	// Update is called once per frame
	void Update () {
        if (isTargeting)
        {
			Vector2 dir = rigidBody.velocity;
			float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
			transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

			Vector2 dist = targeting.GetTargetPoint() - (Vector2)transform.position;
			dist = dist.normalized;

			rigidBody.AddForce(dist * speed * strengthOfAttraction*rigidBody.mass);

            //damage = baseDamage + Mathf.Sqrt(rigidBody.velocity.magnitude/2)/2;
			transform.position = new Vector2(transform.position.x, transform.position.y);
        }
        
    }
}
