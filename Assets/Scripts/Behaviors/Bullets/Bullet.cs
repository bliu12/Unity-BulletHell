﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    public float speed = 20;
    public float damage = 1;
    public float timeout = 2;

    private Rigidbody2D _rigidBody;
    private Rigidbody2D rigidBody { get
        {
            if (_rigidBody == null) _rigidBody = GetComponent<Rigidbody2D>();
            return _rigidBody;
        }
    }
    private Vector2 _moveDirection;
    public virtual Vector2 MoveDirection {
        get { return _moveDirection; }
        set {
            _moveDirection.x = value.x;
            _moveDirection.y = value.y;
            _moveDirection.Normalize();

            rigidBody.AddForce(_moveDirection * speed * rigidBody.mass, ForceMode2D.Impulse);//.velocity = _moveDirection * speed;

        }
    }

    public void Start() {
        if(timeout > 0)
            Destroy(gameObject, timeout);
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        var shields = collision.gameObject.GetComponent<Shields>();
        var healthBar = collision.gameObject.GetComponent<HealthBar>();
        if(shields != null) {
            shields.Hit(damage);
        }
        else if(healthBar != null) {
            healthBar.Hit(damage);
        }

        var hitEffect = Instantiate(Resources.Load(Strings.Resources.BulletHitEffect) as GameObject,
                         gameObject.transform.position,
                         gameObject.transform.rotation);
        Destroy(hitEffect, 0.5F);
        Destroy(gameObject);

    }

}
