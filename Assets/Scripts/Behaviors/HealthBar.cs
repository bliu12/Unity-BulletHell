﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour {
    public float health = 10;
    private float maxHealth;
    public GameObject healthBar;
    private Slider healthSlider;
    public Canvas canvas;
    private float healthPanelOffset = 1;

    // Use this for initialization

    public void Hit(float damage) {
        health -= damage;
    }

    public void Die(){
        health = 0;
    }

	void Start () {
        maxHealth = health;
        if (gameObject.tag != Strings.Tags.Bullet)
        {
            canvas = GameObject.Find("WorldCanvas").GetComponent<Canvas>();
            healthBar = Instantiate(Resources.Load(Strings.Resources.HealthSlider) as GameObject);
            healthBar.transform.SetParent(canvas.transform, false);
            healthSlider = healthBar.GetComponent<Slider>();
            healthSlider.minValue = 0;
            healthSlider.maxValue = maxHealth;
            HealthBarUpdate();
        }
    }

    // Update is called once per frame
    void Update() {

        if (health <= 0)
        {
            var explosion = Instantiate(Resources.Load(Strings.Resources.Explosion) as GameObject,
                                gameObject.transform.position,
                                gameObject.transform.rotation);
            Destroy(explosion, 1F);
            Destroy(healthBar);
            Destroy(gameObject);
        }
        else if (healthSlider) {
            HealthBarUpdate();
        }

    }

    public void HealthBarUpdate()
    {
        healthSlider.value = health;
        Vector3 worldPos = new Vector3(transform.position.x, transform.position.y + healthPanelOffset);
        healthBar.transform.position = worldPos;
    }
}
