﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StandardGun : BaseGun {
    public float timeout = 2;
    public override void Shoot(TargetingBase target)
    {
        var direction = target.GetTargetDirection();
        var deg = Mathf.Rad2Deg * Math.Atan2(direction.y, direction.x);
        int start = -(int)(bulletsAtOnce / 2);
        for (var i = start; i <= start + bulletsAtOnce - 1; i++)
        {
            var bullet = InstantiateBullet();
            var bulletParams = bullet.GetComponent<Bullet>();
            var angle = ((firingCone/bulletsAtOnce) * i + deg);
            if(bulletsAtOnce % 2 == 0)
            {
                angle += (firingCone / bulletsAtOnce) / 2;
            }
            angle *= Mathf.Deg2Rad;
            var dir = new Vector2((float)Math.Cos(angle), (float)Math.Sin(angle));
            bullet.transform.right = dir;
            bulletParams.MoveDirection = dir;
            bulletParams.timeout = timeout;
        }
    }

    public override void Release ()
	{
		
	}
}
