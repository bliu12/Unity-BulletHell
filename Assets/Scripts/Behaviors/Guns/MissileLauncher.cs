﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileLauncher : BaseGun {
    private List<GameObject> Missiles;

    public override void Release()
    {

        Missiles.RemoveAll(x => !(x.gameObject));
        //iterate missiles list and deactivate following on all of them
        foreach(var missile in Missiles)
        {
            missile.GetComponent<Missile>().isTargeting = false;
        }

    }
    public void Start()
    {
        Missiles = new List<GameObject>();
    }

    public override void Shoot(TargetingBase target)
    {
        var direction = target.GetTargetDirection();
        var deg = Mathf.Rad2Deg * Math.Atan2(direction.y, direction.x);
        int start = -(int)(bulletsAtOnce / 2);
        for (var i = start; i <= start + bulletsAtOnce - 1; i++)
        {
            var bullet = InstantiateBullet();
            var missileController = bullet.GetComponent<Missile>();
            missileController.targeting = target;
			bullet.transform.LookAt(target.GetTargetDirection());
            Missiles.Add(bullet);
            var bulletParams = bullet.GetComponent<Bullet>();
            var angle = ((firingCone / bulletsAtOnce) * i + deg);
            if (bulletsAtOnce % 2 == 0)
            {
                angle += (firingCone / bulletsAtOnce) / 2;
            }
            angle += UnityEngine.Random.Range(-firingCone / 2, firingCone / 2);
            angle *= Mathf.Deg2Rad;
            bulletParams.speed = 10;
            bulletParams.MoveDirection = new Vector2((float)Math.Cos(angle), (float)Math.Sin(angle));
            var bulletRigidBody = bullet.GetComponent<Rigidbody2D>();
            bulletRigidBody.AddForce(bulletParams.MoveDirection * 1000 * bulletRigidBody.mass);

            Destroy(bullet, 5F);
        }
    }

}
