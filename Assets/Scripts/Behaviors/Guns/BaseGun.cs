﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseGun : MonoBehaviour
{

    public GameObject bulletType;
    public Transform bulletSpawn;
    public float shotsPerMinute;
    public float firingCone;
    public int bulletsAtOnce;

    public abstract void Shoot(TargetingBase target);
	public abstract void Release();

    protected GameObject InstantiateBullet()
    {
        var bullet = (GameObject)Instantiate(
            bulletType,
            bulletSpawn.position,
            bulletSpawn.rotation);
        bullet.tag = Strings.Tags.Bullet;
        return bullet;
    }
}

