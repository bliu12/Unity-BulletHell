﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser : BaseGun {
	public bool isShooting = false;
	GameObject beam;

    public override void Shoot(TargetingBase target)
    {
        // When the method is first called.
        if (!isShooting) {
			beam = (GameObject)Instantiate (bulletType);
			beam.tag = Strings.Tags.Bullet;
            var laserBeam = beam.GetComponent<Beam>();
            laserBeam.targeting = target;
			isShooting = true;
		} 
    }
    
	public override void Release ()
	{
		isShooting = false;
		Destroy(beam);
	}
}
