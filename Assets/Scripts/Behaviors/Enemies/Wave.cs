﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wave : MonoBehaviour
{


    private class SpawnerHolder{
        public GameObject spawner;
        public Transform transform;

        public SpawnerHolder(GameObject spawn, Transform trans){
            spawner = spawn;
            transform = trans;
        }
    }

    private List<GameObject> enemies;
    private List<SpawnerHolder> wavespawners;

    private float timer;

    // Use this for initialization
    void Start()
    {
        enemies = new List<GameObject>();
        wavespawners = new List<SpawnerHolder>();
    }

    // add a new spawner to the wave.
    public void AddSpawner(GameObject spawn, Transform trans){
        wavespawners.Add(new SpawnerHolder(spawn, trans));
    }

    public void AddEnemies(GameObject enemy){
        enemies.Add(enemy);
    }

    // start the new wave
    public void NewWave(){
        SpawnerHolder holder = wavespawners[0];
        enemies.Add(Instantiate(holder.spawner, holder.transform));
        wavespawners.RemoveAt(0);
    } 

	// Update is called once per frame
	void Update()
	{
        //if out of enemies, generate next wave.
        if(enemies.Count == 0 && wavespawners.Count == 0){
            NewWave();
        }
        //round over
        else if(enemies.Count == 0){
            
        }
	}
}