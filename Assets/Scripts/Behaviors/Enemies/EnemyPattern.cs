﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPattern : MonoBehaviour {

	private Rigidbody2D rigidBody;
	private float strengthOfAttraction = 10;
    private TargetingBase targeting;
	// Use this for initialization
	void Start () {
		rigidBody = GetComponent<Rigidbody2D>();
        targeting = GetComponent<TargetingBase>();
	}
	
	// Update is called once per frame
	void Update () {
        Vector2 offset = targeting.GetTargetDirection();

		float magsqr = offset.sqrMagnitude;

		if(magsqr > 0.0001f){
			rigidBody.AddForce(strengthOfAttraction * offset / magsqr, ForceMode2D.Force);
		}
//		physicsThings.AddForce(new Vector3 (player.transform.position.x * 10, player.transform.position.y * 10, player.transform.position.z));
	}

}
