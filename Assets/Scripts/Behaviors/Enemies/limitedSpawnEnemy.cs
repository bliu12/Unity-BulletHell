﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class limitedSpawnEnemy : MonoBehaviour {

    public int enemiesLeft;
	private float timer;
	public GameObject enemyPrefab;

	// Use this for initialization
	void Start () {
		timer = Random.Range(1, 3);
        enemiesLeft = 100;
		this.enemyPrefab = Resources.Load (Strings.Resources.Enemy) as GameObject;
	}

	// Update is called once per frame
	void Update () {
		timer -= Time.deltaTime;
        if (timer <= 0 && enemiesLeft > 0)
		{
			GameObject enemy = (GameObject)Instantiate(enemyPrefab);
			Rigidbody2D en = enemy.GetComponent<Rigidbody2D>();

			timer = Random.Range(1, 3);
            enemiesLeft--;
		}
        else if( timer <= 0){
            DestroyObject(gameObject);
        }
	}
}
