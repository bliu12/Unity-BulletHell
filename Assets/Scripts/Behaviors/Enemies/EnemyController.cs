﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(HealthBar))]
public class EnemyController : MonoBehaviour {

    HealthBar healthBar;
    TargetingBase targeting;
    Rigidbody2D rigidBody;
	void Start () {
		healthBar = GetComponent<HealthBar>();
        targeting = GetComponent<TargetingBase>();
        rigidBody = GetComponent<Rigidbody2D>();
	}

	// Update is called once per frame
	void Update () {
		gameObject.transform.position = new Vector2 (
			gameObject.transform.position.x,
			gameObject.transform.position.y
		);
        var shooting = GetComponent<Shooting>();
        transform.right = rigidBody.velocity; //((Vector3)targeting.GetTargetPoint() - transform.position);
        shooting.ShootGun();
	}

    void OnCollisionEnter(Collision collision)
    {
        
    }

    private void OnDestroy()
    {

    }
}
