﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnEnemy : MonoBehaviour {

	private float timer;
	public GameObject enemyPrefab;

	// Use this for initialization
	void Start () {
		timer = Random.Range(1, 3);
		this.enemyPrefab = Resources.Load (Strings.Resources.Enemy) as GameObject;
	}

	// Update is called once per frame
	void Update () {
		timer -= Time.deltaTime;
		if (timer <= 0) {
			GameObject enemy = (GameObject)Instantiate (enemyPrefab);
//			Rigidbody en = enemy.GetComponent<Rigidbody>();

//			en.AddForce (new Vector3(Random.Range (-500, 500), Random.Range (-500, 500), 0));

			timer = Random.Range(1, 3);
		}
	}
}
