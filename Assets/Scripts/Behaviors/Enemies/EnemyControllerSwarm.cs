﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(HealthBar))]
public class EnemyControllerSwarm : MonoBehaviour
{
	HealthBar healthBar;
    int damage;
	void Start()
	{
		healthBar = GetComponent<HealthBar>();
        damage = 5;
    }

	// Update is called once per frame
	void Update()
	{
		gameObject.transform.position = new Vector2(
			gameObject.transform.position.x,
			gameObject.transform.position.y
		);
		//var shooting = GetComponent<Shooting>();
		//shooting.ShootGun();
	}

	void OnCollisionEnter(Collision collision)
	{
        if(collision.gameObject.tag == "Player"){
            var shield = collision.gameObject.GetComponent<Shields>();
            var health = collision.gameObject.GetComponent<HealthBar>();
            if(shield != null){
                shield.Hit(damage);
            }
            else if(health != null){
                health.Hit(damage);
            }
            gameObject.GetComponent<HealthBar>().Die();
        }
	}

	private void OnDestroy()
	{

	}
}