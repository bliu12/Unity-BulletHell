﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwarmEnemyPattern : MonoBehaviour {

	private Rigidbody2D physicsThings;
	private float strengthOfAttraction = 20;

	private GameObject player;
	// Use this for initialization
	void Start()
	{
		physicsThings = gameObject.GetComponent<Rigidbody2D>();
		player = GameObject.FindGameObjectWithTag(Strings.Tags.Player);
	}

	// Update is called once per frame
	void Update()
	{
		Vector2 offset = player.transform.position -
			transform.position;

		float magsqr = offset.sqrMagnitude;

		if (magsqr > 0.0001f)
		{
			physicsThings.AddForce(strengthOfAttraction * offset / magsqr, ForceMode2D.Force);
		}
		
    }
}
