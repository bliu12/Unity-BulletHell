﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(HealthBar))]
public class Shields : MonoBehaviour {
    public float shields = 10;
    private float maxShield = 10;
    public float recoveryRate = 1;
    HealthBar healthBarComponent;

    public GameObject shieldBar;
    private Slider shieldSlider;
    public Canvas canvas;
    private float shieldPanelOffset = 2;

    public void Hit(float damage)
    {
        shields -= damage;
        if (shields < 0) {
            healthBarComponent.health += shields;
            shields = 0;
        }

    }
    void Start()
    {
        healthBarComponent = GetComponent<HealthBar>();
        maxShield = shields;
        if (gameObject.tag != Strings.Tags.Bullet)
        {
            canvas = GameObject.Find("WorldCanvas").GetComponent<Canvas>();
            shieldBar = Instantiate(Resources.Load(Strings.Resources.ShieldSlider) as GameObject);
            shieldBar.transform.SetParent(canvas.transform, false);
            shieldSlider = shieldBar.GetComponent<Slider>();
            shieldSlider.minValue = 0;
            shieldSlider.maxValue = maxShield;
        }
    }

    // Update is called once per frame
    void Update () {
        if (shields >= maxShield) {
            shields = maxShield;
        }
        else {
            shields += recoveryRate * Time.deltaTime;
        }

        shieldSlider.value = shields;
        //healthSlider.transform.position = transform.position;
        Vector2 worldPos = new Vector2(transform.position.x, transform.position.y + shieldPanelOffset);
        shieldBar.transform.position = worldPos;

    }
}
