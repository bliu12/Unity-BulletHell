﻿using System.Collections;
using System.Collections.Generic;	
using UnityEngine;



public class Controller : MonoBehaviour {
	private float originalZ;
	private float speedBoost;
	private float speedTimer;
	private bool isSpeeding;
	public float speed = 6.0F;
	private Vector2 moveDirection = Vector2.zero;

    // Use this for initialization
    //private CharacterController controller;
    private Rigidbody2D rigidBody;
    private Shooting shooting;
    private float lastAngle;
	void Start()
	{
        //controller = GetComponent<CharacterController>();
		originalZ = gameObject.transform.position.z;
		speedBoost = speed * 4;
		speedTimer = 0.2F;
		isSpeeding = false;
        shooting = GetComponent<Shooting>();
        rigidBody = GetComponent<Rigidbody2D>();
        setGun(GunType.Standard);
    }
		
    void Update()
    {
		
        moveDirection = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
		if (Input.GetKeyUp(KeyCode.Space) && !isSpeeding) {
			isSpeeding = true;
		}
		if (isSpeeding && speedTimer > 0.0F) {
			moveDirection *= speedBoost;
			speedTimer -= Time.deltaTime;
		} else {
			moveDirection *= speed;
			speedTimer = 0.2F;
			isSpeeding = false;
		}

        float angle = Mathf.Atan2(moveDirection.y, moveDirection.x) * Mathf.Rad2Deg;
        if (moveDirection.magnitude == 0)
            angle = lastAngle;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        rigidBody.AddForce(moveDirection * speed* rigidBody.mass);

        lastAngle = angle;
        if(rigidBody.velocity.magnitude > speed)
            rigidBody.velocity = rigidBody.velocity.normalized * speed;
        if (Input.GetButton("Fire1"))
        {
            shooting.ShootGun();

        }

        if (Input.GetButtonUp("Fire1"))
        {
            shooting.ReleaseGun();
        }

        if (Input.GetKeyUp("1"))
        {
            setGun(GunType.Standard);
        }
        if (Input.GetKeyUp("4"))
        {
            setGun(GunType.Laser);
        }
        if(Input.GetKeyUp("3"))
        {
            setGun(GunType.Missile);
        }
    }


    // wrap the gun changing functionality in this for now until further rewrite
    private void setGun(GunType type)
    {
        var gun = ChangeGun(type);

        shooting.UpdateGun(gun);
    }

    private BaseGun ChangeGun(GunType newGun)
    {
        if (shooting.gun != null) shooting.gun.Release();

        Destroy(GetComponent<BaseGun>());

        BaseGun gun;
        if (newGun == GunType.Laser)
        {
            gun = gameObject.AddComponent<Laser>();
            gun.bulletType = Resources.Load(Strings.Resources.Laser) as GameObject;
            gun.shotsPerMinute = 100000;
        }
        else if(newGun == GunType.Missile)
        {
            gun = gameObject.AddComponent<MissileLauncher>();
            gun.bulletType = Resources.Load(Strings.Resources.Missile) as GameObject;
            gun.shotsPerMinute = 500;
            gun.bulletsAtOnce = 8;
            gun.firingCone = 30;
        }
        else
        {
            gun = gameObject.AddComponent<StandardGun>();
            gun.bulletType = Resources.Load(Strings.Resources.Bullet) as GameObject;
            gun.shotsPerMinute = 1000;
            gun.bulletsAtOnce = 10;
            gun.firingCone = 30;
            ((StandardGun)gun).timeout = 2f;
        }
        gun.bulletSpawn = transform;

        return gun;
    }
}
