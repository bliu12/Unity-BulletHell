﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class TargetingBase : MonoBehaviour {
    public abstract Vector2 GetTargetPoint();
    public Vector2 GetTargetDirection()
    {
        var point = GetTargetPoint();
		return point - (Vector2) gameObject.transform.position;
    }
}
