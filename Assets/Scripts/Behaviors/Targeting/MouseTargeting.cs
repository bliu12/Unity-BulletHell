﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseTargeting : TargetingBase {
    public override Vector2 GetTargetPoint()
    {
        // 30 is how many units the camera is from the player.
        Vector3 worldPoint = Camera.main.ScreenToWorldPoint(new Vector3(
            Input.mousePosition.x,
            Input.mousePosition.y,
            -Camera.main.transform.position.z - transform.position.z
        ));
        return worldPoint;
    }
}
