﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTargeting : TargetingBase {

    protected GameObject player;
    // Use this for initialization
    void Start () {
        player = GameObject.FindGameObjectWithTag(Strings.Tags.Player);
    }

    public override Vector2 GetTargetPoint()
    {
        if (player)
            return player.transform.position;
        else return new Vector2(0, 0);
    }

}
