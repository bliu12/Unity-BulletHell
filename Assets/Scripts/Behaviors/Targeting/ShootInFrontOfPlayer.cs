﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootInFrontOfPlayer : PlayerTargeting
{
    public override Vector2 GetTargetPoint()
    {
        if (player)
            return (Vector2)player.transform.position + player.GetComponent<Rigidbody2D>().velocity;
        else return base.GetTargetPoint();
    }
}
