﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowObject : MonoBehaviour {
    public GameObject followTarget;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if(followTarget)
            transform.position = followTarget.transform.position + new Vector3(0, 0, -30);

	}
}
