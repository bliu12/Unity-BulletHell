﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooting : MonoBehaviour {
    private float cooldown = 0;
	public BaseGun gun;
    public GameObject bulletType;
    public Transform bulletSpawn;
    public TargetingBase targeting;

    void Start () {
		if (this.bulletType == null) {
			this.bulletType = Resources.Load (Strings.Resources.Bullet) as GameObject;
		}
		if (this.bulletSpawn == null) {
			this.bulletSpawn = gameObject.transform;
		}
        if (this.gun == null) {
            setGun(GunType.Standard);
        }
        if (targeting == null)
        {
            this.targeting = gameObject.AddComponent<MouseTargeting>();
        }
	}

    // Update is called once per frame
    void Update()
    {
        if (cooldown > 0)
        {
            cooldown -= Time.deltaTime;
        }

        //		bool shooting = Input.GetButton("Fire1");

        if (Input.GetButton("Fire1") && cooldown <= 0)
        {
            cooldown = 60 / (float)gun.shotsPerMinute;

           
            var point = this.targeting.GetTargetPoint();
            gun.Shoot(point);

        }

        if (Input.GetButtonUp("Fire1"))
        {
            gun.Release();
        }

        if (Input.GetKeyUp("1"))
        {
            setGun(GunType.Standard);
        }
        if (Input.GetKeyUp("4"))
        {
            setGun(GunType.Laser);
        }
    }

    // wrap the gun changing functionality in this for now until further rewrite
    private void setGun(GunType type)
    {
        this.gun = ChangeGun(type);

        switch (type)
        {
            case GunType.Laser:
                this.bulletType = Resources.Load(Strings.Resources.Laser) as GameObject;
                this.gun.shotsPerMinute = 100000;
                break;
            case GunType.Standard:
            default:
                this.bulletType = Resources.Load(Strings.Resources.Bullet) as GameObject;
                this.gun.shotsPerMinute = 1000;
                this.gun.bulletsAtOnce = 10;
                this.gun.firingCone = 30;
                break;
        }
        this.gun.bulletType = bulletType;
        this.gun.bulletSpawn = bulletSpawn;
    }

	private BaseGun ChangeGun(GunType newGun) {
		if(this.gun != null) this.gun.Release ();

		Destroy (GetComponent<BaseGun>());

		if (newGun == GunType.Laser) {
			return gameObject.AddComponent<Laser> ();
		} else {
			return gameObject.AddComponent<StandardGun>();
		}
	}
}
