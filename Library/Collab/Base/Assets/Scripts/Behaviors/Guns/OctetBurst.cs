﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OctetBurst : BaseGun {
    public override void Shoot(Vector3 direction)
    {
        var deg = Mathf.Rad2Deg * Math.Atan2(direction.y, direction.x);
        for (var i = 0; i < 8; i++)
        {
            var bullet = (GameObject)Instantiate(
                BulletType,
                BulletSpawn.position,
                BulletSpawn.rotation);
            bullet.tag = "Bullet";
            var bulletParams = bullet.GetComponent<Bullet>();
            var angle = Mathf.Deg2Rad * (360/8 * i + deg);
            bulletParams.MoveDirection = new Vector3((float)Math.Cos(angle), (float)Math.Sin(angle), 0);
            Destroy(bullet, 2F);
        }
    }
}
