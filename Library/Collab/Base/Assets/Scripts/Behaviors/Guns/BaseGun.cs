﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseGun : MonoBehaviour, IGun
{

    public BaseGun(GameObject bulletType, Transform bulletSpawn) {
        this.bulletSpawn = bulletSpawn;
        this.bulletType = bulletType;
    }

    public GameObject bulletType;
    public GameObject BulletType { get { return bulletType; } set { bulletType = value; } }
    public Transform bulletSpawn;
    public Transform BulletSpawn { get { return bulletSpawn; } set { bulletSpawn = value; } }
    public int shotsPerMinute;
    public int ShotsPerMinute { get { return shotsPerMinute; } set { shotsPerMinute = value; } }

    public abstract void Shoot(Vector3 direction);
}

