﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IGun {
    GameObject BulletType { get; set; }
    Transform BulletSpawn { get; set; }
    int ShotsPerMinute { get; set; }

    void Shoot(Vector3 direction);
}
