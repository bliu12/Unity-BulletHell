﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StandardGun : BaseGun {

    public override void Shoot(Vector3 direction)
    {
        var bullet = (GameObject)Instantiate(
            BulletType,
            BulletSpawn.position,
            BulletSpawn.rotation);
        bullet.tag = "Bullet";
        var bulletParams = bullet.GetComponent<Bullet>();
        bulletParams.MoveDirection = direction;
        Destroy(bullet, 2F);
    }

}
